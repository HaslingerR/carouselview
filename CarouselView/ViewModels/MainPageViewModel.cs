﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace CarouselView.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Main Page";
            CurrentItemChangedCommand = new DelegateCommand<DateWithInfo>(OnCurrentItemChangedCommandExecuted);
            RemainingItemsThresholdReachedCommand = new DelegateCommand(OnRemainingItemsThresholdReachedCommandExecuted);

            Dates = new ObservableCollection<DateWithInfo>();

            Dates.Add(new DateWithInfo { RealDate= DateTime.Today, Date = DateTime.Today.ToString("d"), Info = "" });
            //Dates.Add(new DateWithInfo { RealDate = DateTime.Today.AddDays(1), Date = DateTime.Today.AddDays(1).ToString("d"), Info = "" });
        }

        public DelegateCommand<DateWithInfo> CurrentItemChangedCommand { get; set; }
        public DelegateCommand RemainingItemsThresholdReachedCommand { get; set; }

        public ObservableCollection<DateWithInfo> Dates { get; }


        private DateTime selectedDate;
        public DateTime SelectedDate
        {
            get { return selectedDate; }
            set { SetProperty(ref selectedDate, value); }
        }

        private string lblText;
        public string LblText
        {
            get { return lblText; }
            set { SetProperty(ref lblText, value); }
        }

        private void OnCurrentItemChangedCommandExecuted(DateWithInfo obj)
        {
            obj.Info = obj.Date;
        }


        private void OnRemainingItemsThresholdReachedCommandExecuted()
        {
            DateWithInfo lastDate = Dates.LastOrDefault();
            DateTime newRealDate = lastDate.RealDate.AddDays(1);
            if (lastDate!=null)
            {

            }
            DateWithInfo newDate = new DateWithInfo { RealDate = newRealDate, Date = newRealDate.ToString("d"), Info = "" };
            if (!Dates.Contains(newDate)) {
                Dates.Add(newDate);
            }
        }

    }
}
