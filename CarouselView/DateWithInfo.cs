﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace CarouselView
{
    public class DateWithInfo : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName=null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string date;

        public string Date
        {
            get { return date; }
            set { date = value; OnPropertyChanged(); }
        }

        private DateTime realDate;

        public DateTime RealDate
        {
            get { return realDate; }
            set { realDate = value; OnPropertyChanged(); }
        }

        private string info;

        public string Info
        {
            get { return info; }
            set { info = value; OnPropertyChanged(); }
        }


    }
}
